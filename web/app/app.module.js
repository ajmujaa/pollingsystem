var app = angular.module('app', ['ngRoute', 'ngAnimate', 'ngAria', 'ngMaterial', 'poll', 'about']);

app.config(function($routeProvider) {
    $routeProvider
		.when('/', {
            templateUrl : '/app/poll/poll.html',
            controller  : 'poll'
        })
        .when('/about', {
        	templateUrl: '/app/about/about.html',
        	controller: 'about'
        });
});
