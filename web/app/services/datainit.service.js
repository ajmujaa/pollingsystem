(function(module){

	module.factory('DataInitService', DataInitService);

	DataInitService.$inject = ["$http"];

	function DataInitService($http){

		let initData = () => {
	  		return $http.get('http://localhost:8000/games').then((gamesData) => {
	  			return gamesData.data;
	  		});
		};

		let updateData = (data) => {
			$http.put('http://localhost:8000/games', data).then((result) => {
				console.log(result);
			});
		};

		return {
			initData: initData,
			updateData: updateData
		}
	}



})(angular.module('app'));