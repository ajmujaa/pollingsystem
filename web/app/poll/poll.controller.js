(function (module) {

    'use strict';

    module.controller('poll', ['$scope', '$http', 'DataInitService', function ($scope, $http, DataInitService) {

    	let init = DataInitService.initData().then((gamesData) => {
                $scope.data = gamesData;
            });
        
        let games = $scope.data;

    	let filterGames = gamesList => {
    		return _.filter($scope.data, game => {
    			return game.gameState !== 'FINISHED';
    		});
    	};
    	$scope.game = _.first(_.sample(filterGames(games), 1));

    	$scope.isResultsVisible = false;
    	$scope.selectedTeam = { team: ''};
    	$scope.vote = (game, type) => {
    		let away = game.awayVote !== null ? game.awayVote : 0;
    		let home = game.homeVote !== null ? game.homeVote : 0;
    		let draw = game.drawVote !== null ? game.drawVote : 0;
    		switch(type){
    			case 'away': away += 1;break;
    			case 'home': home += 1;break;
    			case 'draw': draw += 1;break;
    		}
    		game.homeVote = home;
    		game.awayVote = away;
    		game.drawVote = draw;
            updateGame(JSON.stringify(game));
    		$scope.isResultsVisible = true;
    	};

        let updateGame = (game) => {
            DataInitService.updateData(game);
        };

    	$scope.next = () => {
    		$scope.isResultsVisible = false;
    		$scope.game = _.first(_.sample(filterGames(games), 1));
    		$scope.sport = $scope.game.sport;
            $scope.selectedTeam = { team: ''};
    	};
    }]);

})(angular.module('poll'));