let gulp = require('gulp'),
    server = require('gulp-server-livereload'),
    fs = require('fs'),
    apiServer = require('gulp-express');
    conn = require('./../api/connection.js');

let readFile = () => {
    console.log("Starting to read file");
    let contents = JSON.parse(fs.readFileSync('./web/gameData/test-assignment.json'));

    for(let content of contents){
        let query = "INSERT INTO poll.games (id, awayName, gameGroup, homeName, name, objectId, sport, country, gameState, created_at ) VALUES " + 
                    "('"+content.id+"', '"+content.awayName+"', '"+content.group+"', '"+content.homeName+"', '"+content.name+"', '"+content.objectId+"', '"+content.sport+"', '"+content.country+"', '"+content.state+"', '"+content.createdAt+"')";
        conn.query(query, content, (err, res) => {
            if(err){
                console.log("Error : " + err);
            }else{
                console.log("Game " + content.name + " has successfully added");
            }
        });
    }
};
// module.exports.readFile = readFile;

let createDb = () => {
    console.log("Creating the db");

    let query = "CREATE DATABASE poll";
    conn.query(query, (err, rows, field) => {
        if(!err){
            console.log("Creating the table");
            let createTable = "CREATE TABLE poll.games(id INT NULL, awayName VARCHAR(50) NULL," +
            "homeName VARCHAR(50) NULL, gameGroup VARCHAR(50) NULL, " +
            "name VARCHAR(50) NULL, objectId VARCHAR(50) NULL, " +
            "sport VARCHAR(50) NULL, country VARCHAR(50)NULL, " + 
            "gameState VARCHAR(50) NULL, homeVote INT NULL DEFAULT 0, awayVote INT NULL DEFAULT 0, drawVote INT NULL DEFAULT 0,"+ 
            "created_at TIMESTAMP NULL) COLLATE='latin1_swedish_ci' ENGINE=InnoDB";
            return conn.query(createTable, (err, rows, field) => {
                if(!err){
                    readFile();
                }else{
                    console.log("Error in selecting DB : " + err);
                }
            });
        }else{
            console.log("Error in creating database : " + err);
        }
    });
}
module.exports.createDb = createDb;

let buildCSS = () => {
	gulp.task('sass', () =>{
		return gulp.src('/stylesheets/*.css')
			.pipe(sass())
			.pipe(gulp.dest('build/css'));
	});
};
module.exports.buildCSS = buildCSS;

let webServer = () => {
    apiServer.run(['./api/app.js']);
    gulp.src('./web').pipe(server({
        proxies: [{
            source: './api',
            target: 'http://localhost:8000'
        }],
        livereload: true,
        port: 8080,
        open: true,
        host: 'localhost',
        defaultFile: 'index.html'
    }));

};
module.exports.webServer = webServer;
