/**
 *  Welcome to your gulpfile!
 *  The gulp tasks are splitted in several files in the gulp directory
 *  because putting all here was really too long
 */

'use strict';

var gulp = require('gulp'),
    buildHelper = require('./build/helper');


gulp.task('db-create', buildHelper.createDb);    

gulp.task('read-file', ['db-create'], buildHelper.readFile);

gulp.task('init-db', ['db-create', 'read-file']);

gulp.task('build-css', buildHelper.buildCSS);

/* Start local web server */
gulp.task('server', buildHelper.webServer);
