# README #

## About the Sports Poll System ##

The application is to randomly load a team from the given games file and record the votes and show the number of votes recieved.

### The Project ###

The following technologies have been used in the application

* Angular JS
* HTML5 / HTML5 Localstorage
* CSS3 / SCSS / Bootstrap
* Underscore JS
* Bootstrap JS

The images are taken from the free sources

### Instructions to run the project ###

1. Clone the project to local machine
2. Open the project folder in command line and type npm install
3. Once the dependencies installed, type gulp server
4. This will run the node server and open the website in the browser automatically
5. You will be landing on the Poll page

### How is the project works? ###

When you run the project for the first time, the sample data from the JSON file will be saved in the browser's localStorage. When you poll, the Vote button will be activated. When the vote is done, the result will be shown with the number of votes recived in a progress bar component. In the bottom, there's a Next button, when it get pressed, another game data randomly will be taken and loaded to the page. If you want, you can refresh the browser refresh button to load a new item randomly