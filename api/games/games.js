let conn = require('./../connection');

conn.connect((err) => {
	if(!err){
		console.log("DB connected");
	}else{
		console.log("Error : "+ err);
	}
});

module.exports.get = (res) => {
	conn.query('SELECT * FROM poll.games', (err, rows, field) => {
		let games = [];
		if(!err){
			games.push(JSON.stringify(rows));
		}else{
			console.log(err);
		}
		res.send(JSON.parse(games));
	});
};

module.exports.update = (game, res) => {
	console.log(game);
	conn.query("UPDATE poll.games SET homeVote=?, awayVote=?, drawVote=? WHERE id = ?", [game.homeVote, game.awayVote, game.drawVote, game.id] ,(err, rows, field) => {
		if(!err){
			console.log(JSON.stringify(rows));
		}else{
			console.log("Error : " + err);
		}
	});
};
