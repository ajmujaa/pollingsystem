let express = require('express');
let bodyParser = require('body-parser');
let routes = require('./routes');
let cors = require('cors');

let app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

let corsOptions = {
  origin: '*',
  optionsSucccessCode: 200 
};

routes.configure(app, corsOptions);

let server = app.listen(8000, () =>{
	console.log('Server listening on port : ' + server.address().port);
});