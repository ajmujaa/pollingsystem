let games = require('./games/games');
let cors = require('cors');

module.exports = {
	configure : (app, corsOptions) => {
		app.get('/games/', cors(corsOptions), (req, res) => {
			games.get(res);
		});

		app.options('/games/', cors());
		app.put('/games/', (req, res) => {
			games.update(req.body, res);
		});
	}
};